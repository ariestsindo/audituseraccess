﻿using System;

namespace PowerApps.Samples
{
    public class AuditTrailModel
    {
        public Guid auditId { get; set; }
        public AuditDetailsModel details { get; set; }
    }
}