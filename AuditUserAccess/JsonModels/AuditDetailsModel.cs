﻿using System;

namespace PowerApps.Samples
{
    public class AuditDetailsModel
    {
        public DateTime createdOn { get; set; }
        public string operation { get; set; }
        public string username { get; set; }
        public string userId { get; set; }

    }
}