﻿using System;
using System.Collections.Generic;

namespace PowerApps.Samples
{
    public class NxlogJsonInputModel
    {
        public string auditType { get; set; }
        public DateTime runningDate { get; set; }
        public string odataFilters { get; set; }
        public List<AuditTrailModel> auditTrails { get; set; }
    }
}
