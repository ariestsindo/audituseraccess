﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace PowerApps.Samples
{
   public partial class SampleProgram
    {
        [STAThread] // Added to support UX
        static void Main(string[] args)
        {
            CrmServiceClient service = null;
            try
            {
                service = SampleHelpers.Connect("Connect");
                if (service.IsReady)
                {
                    #region Sample Code
                    #region Show Audited Records

                    // Select all columns for convenience.
                    var query = new QueryExpression(Audit.EntityLogicalName)
                    {
                        ColumnSet = new ColumnSet(true),
                        Criteria = new FilterExpression(LogicalOperator.And)
                    };

                    // Only retrieve audit records that track user access.
                    query.Criteria.AddCondition("action", ConditionOperator.In,
                        (int)AuditAction.UserAccessAuditStarted,
                        (int)AuditAction.UserAccessAuditStopped,
                        (int)AuditAction.UserAccessviaWebServices,
                        (int)AuditAction.UserAccessviaWeb);

                    // Change this to false in order to retrieve audit records for all users
                    // when running the sample.
                    var filterAuditsRetrievedByUser = false;
                    if (filterAuditsRetrievedByUser)
                    {
                        // Only retrieve audit records for the current user or the "SYSTEM"
                        // user.
                        var userFilter = new FilterExpression(LogicalOperator.Or);
                        userFilter.AddCondition(
                            "userid", ConditionOperator.Equal, _systemUserId);
                        userFilter.AddCondition(
                            "useridname", ConditionOperator.Equal, "SYSTEM");
                    }
                    // Only retrieve records for this sample run, so that we don't get too
                    // many results if auditing was enabled previously.
                    //query.Criteria.AddCondition(
                    //    "createdon", ConditionOperator.GreaterEqual, DateTime.UtcNow);

                    var results = service.RetrieveMultiple(query);
                    Console.WriteLine("Retrieved audit records:");

                    var auditTrails = new List<AuditTrailModel>();
                    auditTrails.Clear();
                    foreach (Audit audit in results.Entities)
                    {
                        //Console.Write("\r\n  Action: {0},  User: {1},"
                        //    + "\r\n    Created On: {2}, Operation: {3}",
                        //    (AuditAction)audit.Action.Value,
                        //    audit.UserId.Name,
                        //    audit.CreatedOn.Value.ToLocalTime(),
                        //    (AuditOperation)audit.Operation.Value);

                        //// Display the name of the related object (which will be the user
                        //// for audit records with Action UserAccessviaWebServices.
                        //if (!String.IsNullOrEmpty(audit.ObjectId.Name))
                        //{
                        //    Console.WriteLine(
                        //        ",\r\n    Related Record: {0}", audit.ObjectId.Name);
                        //}
                        //else
                        //{
                        //    Console.WriteLine();
                        //}

                        var auditTrail = new AuditTrailModel
                        {
                            auditId = audit.Id
                        };

                        var auditDetail = new AuditDetailsModel
                        {
                            username = audit.UserId.Name,
                            userId = audit.UserId.Id.ToString(),
                            createdOn = audit.CreatedOn.Value.ToLocalTime(),
                            operation = ((AuditOperation)audit.Operation.Value).ToString()
                        };

                        auditTrail.details = auditDetail;

                        auditTrails.Add(auditTrail);
                    }

                    var nxlogJson = new NxlogJsonInputModel
                    {
                        auditType = "User Access",
                        odataFilters = "",
                        runningDate = DateTime.UtcNow.ToLocalTime(),
                        auditTrails = auditTrails
                    };

                    string fileName = "NxlogJsonInputModel.json";

                    var options = new JsonSerializerOptions { WriteIndented = true };
                    string jsonString = JsonSerializer.Serialize(nxlogJson, options);
                    File.WriteAllText(fileName, jsonString);

                    #endregion Show Audited Records
                    #endregion
                }


                else
                {
                    const string UNABLE_TO_LOGIN_ERROR = "Unable to Login to Microsoft Dataverse";
                    if (service.LastCrmError.Equals(UNABLE_TO_LOGIN_ERROR))
                    {
                        Console.WriteLine("Check the connection string values in cds/App.config.");
                        throw new Exception(service.LastCrmError);
                    }
                    else
                    {
                        throw service.LastCrmException;
                    }
                }
            }

            catch (Exception ex)
            {
                SampleHelpers.HandleException(ex);
            }

            finally
            {
                if (service != null)
                    service.Dispose();

                Console.WriteLine("Press <Enter> to exit.");
                Console.ReadLine();
            }
        }
    }
}
